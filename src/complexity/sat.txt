A certain question has the following possible answers.

    All of the below
    None of the below
    All of the above
    One of the above
    None of the above
    None of the above

Which answer is correct?
